import tabula
import csv
import os
import json
import re

if not os.path.exists("../testdata/talents.csv"):
	tabula.convert_into("../testdata/Talente.pdf", "../testdata/talents.csv", output_format="csv", pages='all', columns=[147,255,290,380,406,545], guess=False)

category_map = {
	"Kampftalente": "combat",
	"Körperliche Talente": "physical",
	"Gesellschaftliche Ta": "social",
	"Natur-Talente": "nature",
	"Wissenstalente": "knowledge",
	"Sprachen und Schrift": "language",
	"Handwerkstalen": "crafting",
	"Aventurische S": None,
}

combat_category_map = {
	"Bewaffneter Nahkampf": "close_combat",
	"Fernkampf": "ranged_combat",
	"Waffenlos": "unarmed",
	"Bew. Kampf; AT-Technik": "special",
}

type_map = {
	"Basis": "base",
	"Spezial": "special",
}

current_category = None

talents = []

def find_key(entry):
	for key in category_map.keys():
		if key in entry:
			return key

skip_next = True

with open('../testdata/talents.csv', newline='') as csvfile:
	talentreader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in talentreader:
		#print(', '.join(row))
		entry = row[0]
		if any(key in entry for key in category_map.keys()):
			current_category = category_map[find_key(entry)]
			skip_next = True
			#print(current_category)
		elif current_category is not None:
			if skip_next:
				skip_next = False
			elif row[0] != "":
				talent = {}
				talent["name"] = row[0]
				talent["type"] = "talent"
				talent["data"] = {}
				talent["data"]["category"] = current_category
				talent["data"]["talent_type"] = type_map[row[2]]
				if current_category == "combat":
					talent["data"]["combat_category"] = combat_category_map[row[1]]
					talent["data"]["handicap"] = row[4]
				else:
					if current_category == "physical":
						talent["data"]["handicap"] = row[4]
					attributes = re.search(r'\((.*?)\)',row[1])
					attributes = attributes.group(1).split("/")
					talent["data"]["attributes"] = attributes
				talents.append(talent)

					
with open('../testdata/talents.json', 'w') as f:
    json.dump(talents, f)