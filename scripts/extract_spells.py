import tabula
import csv
import os
import json
import re

if not os.path.exists("../testdata/spells.csv"):
	tabula.convert_into("../testdata/Zauber.pdf", "../testdata/spells.csv", output_format="csv", pages='all', columns=[129,172,196,230,258,265,310,350,509,555,566,770], guess=False)

spells = []
skip = False

with open('../testdata/spells.csv', newline='') as csvfile:
	spellreader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in spellreader:
		if row[0] != "" and row[0] != "Name" and row[0] != "Anhang" and not skip:
			spell = {}
			if row[0] == "Abkürzungen:":
				skip = True
			if not skip:
				print(row[0])
				spell["name"] = row[0]
				spell["type"] = "spell"
				spell["data"] = {}
				if row[1].find("+") != -1:
					mod = row[1].split("+")[1]
					spell["data"]["mod"] = mod
					attributes = row[1].split("+")[0]
				else:
					attributes = row[1]
					spell["data"]["mod"] = 0
				attributes = attributes.split("/")
				spell["data"]["attributes"] = attributes
				spell["data"]["cast_time"] = row[2]
				spell["data"]["cost"] = row[3]
				spell["data"]["target"] = row[4]
				spell["data"]["range"] = row[6]
				spell["data"]["duration"] = row[7]
				spell["data"]["lcd_page"] = row[10] 
				spells.append(spell)

					
with open('../testdata/spells.json', 'w') as f:
    json.dump(spells, f)