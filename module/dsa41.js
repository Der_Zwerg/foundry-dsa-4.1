/**
 * A simple and flexible system for world-building using an arbitrary collection of character and item attributes
 * Author: Atropos
 * Software License: GNU GPLv3
 */

// Import Modules
import { Dsa41Actor } from "./actor.js";
import { Dsa41ItemSheet } from "./item-sheet.js";
import { Dsa41ActorSheet } from "./actor-sheet.js";
import { preloadHandlebarsTemplates } from "./preloadTemplates.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
  console.log(`Initializing DSA 4.1 System`);

	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
	CONFIG.Combat.initiative = {
	  formula: "1d20",
    decimals: 2
  };

	// Define custom Entity classes
  CONFIG.Actor.entityClass = Dsa41Actor;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("dsa41", Dsa41ActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("dsa41", Dsa41ItemSheet, {makeDefault: true});

  await preloadHandlebarsTemplates();
  // Register system settings
  game.settings.register("dsa41", "macroShorthand", {
    name: "Shortened Macro Syntax",
    hint: "Enable a shortened macro syntax which allows referencing attributes directly, for example @str instead of @attributes.str.value. Disable this setting if you need the ability to reference the full attribute model, for example @attributes.str.label.",
    scope: "world",
    type: Boolean,
    default: true,
    config: true
  });

  //await importTalents();
  //await importSpells();
});

async function importTalents() {
  // Reference a Compendium pack by it's callection ID
  //const pack = game.packs.find(p => p.collection === `dsa41.talents`);
  const pack = game.packs.get(`dsa41.talents`);
  pack.locked = false;


  // Load an external JSON data file which contains data for import
  const response = await fetch("systems/dsa41/testdata/talents.json");
  const content = await response.json();
  console.log("JSON Entries in Talents: " + content.length);

  // Create temporary Actor entities which impose structure on the imported data
  const talents = await Item.create(content, {temporary: true});

  console.log("Item Entries in Talents: " + talents.length);

  // Save each temporary Actor into the Compendium pack
  for ( let t of talents ) {
    await pack.importEntity(t);
    console.log(`Imported talent ${t.name} into Compendium pack ${pack.collection}`);
  }
}

async function importSpells() {
  // Reference a Compendium pack by it's callection ID
  const pack = game.packs.get(`dsa41.spells`);
  pack.locked = false;


  // Load an external JSON data file which contains data for import
  const response = await fetch("systems/dsa41/testdata/spells.json");
  const content = await response.json();
  console.log("JSON Entries in Spells: " + content.length);

  // Create temporary Actor entities which impose structure on the imported data
  const spells = await Item.create(content, {temporary: true});

  console.log("Item Entries in Spells: " + spells.length);
  // Save each temporary Actor into the Compendium pack
  for ( let s of spells ) {
    await pack.importEntity(s);
    console.log(`Imported spell ${s.name} into Compendium pack ${pack.collection}`);
  }
}

Hooks.on('createActor', async (actor, options, userId) => {
  if (actor.data.type === 'character' && options.renderSheet) {
      let talentIndex = (await game.packs
          .get('dsa41.talents')
          .getContent());
      talentIndex = talentIndex.filter((i) => (i.data.data.talent_type == "base"));
      actor.createEmbeddedEntity('OwnedItem', talentIndex);
  }
});